<?php
/*
* Plugin Name:				Spice Cookies
* Plugin URI:  			
* Description:              This plugin allows you to display notification message that informs site users to use cookies for your website.
* Version:     				1.0
* Requires at least:        5.3
* Requires PHP: 			5.2
* Tested up to: 			6.2
* Author:      				Spicethemes
* Author URI:  				https://spicethemes.com
* License: 				  	GPLv2 or later
* License URI: 				https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 				spice-cookies
* Domain Path:  			/languages
*/

if ( ! function_exists( 'sc_fs' ) ) {
    // Create a helper function for easy SDK access.
    function sc_fs() {
        global $sc_fs;

        if ( ! isset( $sc_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $sc_fs = fs_dynamic_init( array(
                'id'                  => '10574',
                'slug'                => 'spice-cookies',
                'premium_slug'        => 'spice-cookies',
                'type'                => 'plugin',
                'public_key'          => 'pk_bd8fd588af45f7d46543f8a297c04',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
               ) );
        }

        return $sc_fs;
    }
  sc_fs();
}


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// define the constant for the URL
define( 'SPICE_COOKIES_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SPICE_COOKIES_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

add_action( 'init', 'spice_cookies_load_textdomain' );
/**
 * Load plugin textdomain.
 */
function spice_cookies_load_textdomain() {
  load_plugin_textdomain( 'spice-cookies', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}


//Define the function for the plugin activation
function spice_cookies_activation() {

	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/customizer/customizer-class.php';

	//Font file
	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/spice-cookies-fonts.php';

	//Cookies
	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/cookie-app.php';

}
add_action('plugins_loaded', 'spice_cookies_activation');