# Spice Cookies
# Copyright 2022 ...
# This file is distributed under the GNU General Public License v3 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: "
"Blank WordPress Pot "
"v1.0.0\n"
"Report-Msgid-Bugs-To: "
"Translator Name "
"<translations@example."
"com>\n"
"POT-Creation-Date: "
"2021-12-30 17:28+0530\n"
"PO-Revision-Date: \n"
"Last-Translator: Your "
"Name <you@example.com>\n"
"Language-Team: Your Team "
"<translations@example."
"com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/"
"plain; charset=UTF-8\n"
"Content-Transfer-"
"Encoding: 8bit\n"
"Plural-Forms: "
"nplurals=2; plural=n != "
"1;\n"
"X-Textdomain-Support: "
"yesX-Generator: Poedit "
"1.6.4\n"
"X-Poedit-SourceCharset: "
"UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;esc_html_e;"
"esc_html_x:1,2c;"
"esc_html__;esc_attr_e;"
"esc_attr_x:1,2c;"
"esc_attr__;_ex:1,2c;"
"_nx:4c,1,2;"
"_nx_noop:4c,1,2;_x:1,2c;"
"_n:1,2;_n_noop:1,2;"
"__ngettext:1,2;"
"__ngettext_noop:1,2;_c,"
"_nc:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit "
"3.0.1\n"
"X-Poedit-"
"SearchPath-0: .\n"

#: inc/cookie-app.php:8
#: inc/customizer/customizer.php:36
msgid ""
"This website uses "
"cookies to ensure you "
"get the best experience "
"on our website."
msgstr ""

#: inc/cookie-app.php:9
#: inc/customizer/customizer.php:54
msgid "Accept"
msgstr ""

#: inc/customizer/customizer.php:19
msgid "Cookies Settings"
msgstr ""

#: inc/customizer/customizer.php:27
msgid "General Settings"
msgstr ""

#: inc/customizer/customizer.php:43
msgid "Content"
msgstr ""

#: inc/customizer/customizer.php:61
msgid "Button Text"
msgstr ""

#: inc/customizer/customizer.php:79
msgid ""
"Expire Time(Default 15 "
"Days)"
msgstr ""

#: inc/customizer/customizer.php:102
msgid "Typography Settings"
msgstr ""

#: inc/customizer/customizer.php:117
#: inc/customizer/customizer.php:366
msgid ""
"Enable to apply the "
"settings"
msgstr ""

#: inc/customizer/customizer.php:132
msgid "Content Typography"
msgstr ""

#: inc/customizer/customizer.php:159
#: inc/customizer/customizer.php:268
msgid "Font family"
msgstr ""

#: inc/customizer/customizer.php:177
#: inc/customizer/customizer.php:286
msgid "Font size (px)"
msgstr ""

#: inc/customizer/customizer.php:193
#: inc/customizer/customizer.php:302
msgid "Font Weight"
msgstr ""

#: inc/customizer/customizer.php:209
#: inc/customizer/customizer.php:318
msgid "Font style"
msgstr ""

#: inc/customizer/customizer.php:225
#: inc/customizer/customizer.php:334
msgid "Text Transform"
msgstr ""

#: inc/customizer/customizer.php:241
msgid "Button Typography"
msgstr ""

#: inc/customizer/customizer.php:351
msgid "Color Settings"
msgstr ""

#: inc/customizer/customizer.php:375
msgid "Menu"
msgstr ""

#: inc/customizer/customizer.php:387
msgid "Background Color"
msgstr ""

#: inc/customizer/customizer.php:402
msgid "Text Color"
msgstr ""

#: inc/customizer/customizer.php:417
msgid ""
"Button Background Color"
msgstr ""

#: inc/customizer/customizer.php:432
msgid "Button Text Color"
msgstr ""

#: inc/customizer/customizer.php:448
msgid "Button Hover Color"
msgstr ""

#: inc/customizer/customizer.php:463
msgid ""
"Button Hover Text Color"
msgstr ""
