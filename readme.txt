=== Spice Cookies ===

Contributors: 		spicethemes
Tags: 				cookie, cookies, cookie notification
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.2
Stable tag: 		1.0
License: 			GPLv2 or later
License URI: 		http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin allows you to display notification message that informs site users to use cookies for your website.

== Changelog ==

@Version 1.0
* Updated freemius directory.

@Version 0.2
* Freemius Code updated in the main plugin file.

@Version 0.1
* Initial Release.

======= External Resources =======

Js Cookie:
Copyright: Copyright (c) 2018 Copyright 2018 Klaus Hartl, Fagner Brack, GitHub Contributors
License: MIT License
Source: https://github.com/carhartl/jquery-cookie