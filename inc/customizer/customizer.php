<?php
// Adding customizer settings
function spice_cookies_customizer_controls($wp_customize)
{
    $spice_cookies_font_size = array();
    for($i=10; $i<=100; $i++) {
      $spice_cookies_font_size[$i] = $i;
    }
    $spice_cookies_font_style = array('normal'=>'Normal','italic'=>'Italic');
    $spice_cookies_text_transform = array('default'=>'Default','capitalize'=>'Capitalize','lowercase'=>'Lowercase','uppercase'=>'Uppercase');
    $spice_cookies_font_weight = array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900');
    $spice_cookies_font_family = spice_cookies_typo_fonts();
 
    /***** Cookies PANEL *****/
    $wp_customize->add_panel('spice_cookies_panel',
        array(
            'priority' => 141,
            'capability' => 'edit_theme_options',
            'title' => esc_html__('Cookies Settings','spice-cookies')
        ));


    /* ===================================================================================================================
    * Cookies General Setting *
    ====================================================================================================================== */
    $wp_customize->add_section( 'spice_cookies_customizer_section' , array(
        'title'      => esc_html__('General Settings', 'spice-cookies'),
        'priority'   => 1,
        'panel'  => 'spice_cookies_panel',
    ) );


    // Cookie Content
    $wp_customize->add_setting('spice_cookies_content',
        array(
            'default'           =>  esc_html__( 'This website uses cookies to ensure you get the best experience on our website.', 'spice-cookies' ),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'sanitize_text_field'
        )
    );
    $wp_customize->add_control('spice_cookies_content', 
        array(
            'label'             =>  esc_html__('Content','spice-cookies' ),
            'section'           =>  'spice_cookies_customizer_section',
            'setting'           =>  'spice_cookies_content',
            'type'              =>  'textarea',
            'priority'          =>  1,
        )
    );

    // Cookie Button
    $wp_customize->add_setting('spice_cookies_button',
        array(
            'default'           =>  esc_html__( 'Accept', 'spice-cookies' ),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'sanitize_text_field'
        )
    );
    $wp_customize->add_control('spice_cookies_button', 
        array(
            'label'             =>  esc_html__('Button Text','spice-cookies' ),
            'section'           =>  'spice_cookies_customizer_section',
            'setting'           =>  'spice_cookies_button',
            'type'              =>  'text',
            'priority'          =>  2,
        )
    );

    // Cookies expiry time
    $wp_customize->add_setting('spice_cookies_expire',
        array(
            'default'           =>  15,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control('spice_cookies_expire', 
        array(
            'label'             =>  esc_html__('Expire Time(Default 15 Days)','spice-cookies' ),
            'section'           =>  'spice_cookies_customizer_section',
            'setting'           =>  'spice_cookies_expire',
            'type'              =>  'number',
            'input_attrs' => array(
            'min' => 1,
            'max' => 10000000
        )
            
        )
    );
    /* ===================================================================================================================
    * End Cookies General Setting *
    ====================================================================================================================== */



    /* ===================================================================================================================
    * Start Cookies Typo*
    ====================================================================================================================== */

    $wp_customize->add_section('spice_cookies_typo_section', 
        array(
            'title'     => esc_html__('Typography Settings', 'spice-cookies' ),
            'panel'  => 'spice_cookies_panel',
            'priority'  => 2
        )
    );

    $wp_customize->add_setting('enable_cookies_typo',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_cookies_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Cookie_Toggle_Control( $wp_customize, 'enable_cookies_typo',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-cookies'  ),
            'section'   =>  'spice_cookies_typo_section',
            'setting'   =>  'enable_cookies_typo',
            'type'      =>  'toggle'
        )
    ));


    class Spice_Cookies_Text_Typo_Customize_Control extends WP_Customize_Control {
        public $type = 'spice_cookies_text';
        /**
        * Render the control's content.
        */
        public function render_content() {
        ?>
         <h3><?php esc_html_e('Content Typography','spice-cookies'); ?></h3>
        <?php
        }
    }
    $wp_customize->add_setting(
        'spice_content_typo',
        array(
            'capability'     => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )   
    );
    $wp_customize->add_control( new Spice_Cookies_Text_Typo_Customize_Control( $wp_customize, 'spice_content_typo', array( 
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_content_typo',
            'active_callback'   =>  'spice_cookies_typo_callback',
        ))
    );

    $wp_customize->add_setting(
    'spice_cookies_fontfamily',
    array(
        'default'           =>  'Poppins',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'sanitize_text_field',
        )   
    );
    $wp_customize->add_control('spice_cookies_fontfamily', array(
            'label' => esc_html__('Font family','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_cookies_fontfamily',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_family,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));

    $wp_customize->add_setting(
    'spice_cookies_fontsize',
    array(
        'default'           =>  16,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );

    $wp_customize->add_control('spice_cookies_fontsize', array(
            'label' => __('Font size (px)','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_cookies_fontsize',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_size,
            'active_callback'   =>  'spice_cookies_typo_callback',
        ));
    $wp_customize->add_setting(
        'spice_cookies_fontweight',
        array(
            'default'           =>  400,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_cookies_fontweight', array(
            'label' => __('Font Weight','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_cookies_fontweight',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_weight,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
    $wp_customize->add_setting(
        'spice_cookies_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_cookies_fontstyle', array(
            'label' => __('Font style','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_cookies_fontstyle',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_style,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
    $wp_customize->add_setting(
        'spice_cookies_transform',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_cookies_transform', array(
            'label' => __('Text Transform','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_cookies_transform',
            'type'    =>  'select',
            'choices'=>$spice_cookies_text_transform,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
     

    class Spice_Cookies_Btn_Typo_Customize_Control extends WP_Customize_Control {
        public $type = 'spice_cookies_btn';
        /**
        * Render the control's content.
        */
        public function render_content() {
        ?>
         <h3><?php esc_html_e('Button Typography','spice-cookies'); ?></h3>
        <?php
        }
    }
    $wp_customize->add_setting(
        'spice_btn_typo',
        array(
            'capability'     => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )   
    );
    $wp_customize->add_control( new Spice_Cookies_Btn_Typo_Customize_Control( $wp_customize, 'spice_btn_typo', array( 
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_typo',
            'active_callback'   =>  'spice_cookies_typo_callback',
        ))
    );

    $wp_customize->add_setting(
    'spice_btn_fontfamily',
    array(
        'default'           =>  'Poppins',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'sanitize_text_field',
        )   
    );
    $wp_customize->add_control('spice_btn_fontfamily', array(
            'label' => esc_html__('Font family','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_fontfamily',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_family,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));

    $wp_customize->add_setting(
    'spice_btn_fontsize',
    array(
        'default'           =>  16,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );

    $wp_customize->add_control('spice_btn_fontsize', array(
            'label' => __('Font size (px)','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_fontsize',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_size,
            'active_callback'   =>  'spice_cookies_typo_callback',
        ));
    $wp_customize->add_setting(
        'spice_btn_fontweight',
        array(
            'default'           =>  400,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_btn_fontweight', array(
            'label' => __('Font Weight','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_fontweight',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_weight,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
    $wp_customize->add_setting(
        'spice_btn_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_btn_fontstyle', array(
            'label' => __('Font style','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_fontstyle',
            'type'    =>  'select',
            'choices'=>$spice_cookies_font_style,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
    $wp_customize->add_setting(
        'spice_btn_transform',
        array(
            'default'           =>  'uppercase',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_cookies_sanitize_select',
        )   
    );
    $wp_customize->add_control('spice_btn_transform', array(
            'label' => __('Text Transform','spice-cookies'),
            'section' => 'spice_cookies_typo_section',
            'setting' => 'spice_btn_transform',
            'type'    =>  'select',
            'choices'=>$spice_cookies_text_transform,
            'active_callback'   =>  'spice_cookies_typo_callback',
    ));
    /* ===================================================================================================================
    * End Cookies Typo *
    =================================================================================================================== */


    /* ===================================================================================================================
    * Cookies Color Setting 
    =================================================================================================================== */
    $wp_customize->add_section('spice_cookies_clr_section', 
        array(
            'title'     => esc_html__('Color Settings', 'spice-cookies' ),
            'panel'  => 'spice_cookies_panel',
            'priority'  => 2
        )
    );
    
    $wp_customize->add_setting('enable_cookies_clr',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_cookies_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Cookie_Toggle_Control( $wp_customize, 'enable_cookies_clr',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-cookies'  ),
            'section'   =>  'spice_cookies_clr_section',
            'setting'   =>  'enable_cookies_clr',
            'type'      =>  'toggle'
        )
    ));
    
    class Spice_Cookies_Menu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Menu', 'spice-cookies' ); ?></h3>
        <?php }
    }

     $wp_customize->add_setting('spice_cookies_bg_color', 
        array(
            'default'           => '#000000',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new Spice_Cookie_Alpha_Color_Control($wp_customize, 'spice_cookies_bg_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_bg_color'
        )
    ));

    $wp_customize->add_setting('spice_cookies_text_color', 
        array(
            'default'           => '#858585',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_cookies_text_color', 
        array(
            'label'             =>  esc_html__('Text Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_text_color'
        )
    ));
    
    $wp_customize->add_setting('spice_cookies_btn_bg_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_cookies_btn_bg_color', 
        array(
            'label'             =>  esc_html__('Button Background Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_btn_bg_color'
        )
    ));

    $wp_customize->add_setting('spice_cookies_btn_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_cookies_btn_color', 
        array(
            'label'             =>  esc_html__('Button Text Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_btn_color'
        )
    ));


    $wp_customize->add_setting('spice_cookies_btn_bg_colorr', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_cookies_btn_bg_colorr', 
        array(
            'label'             =>  esc_html__('Button Hover Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_btn_bg_colorr'
        )
    ));

    $wp_customize->add_setting('spice_cookies_btn_colorr', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_cookies_btn_colorr', 
        array(
            'label'             =>  esc_html__('Button Hover Text Color', 'spice-cookies' ),
            'active_callback'   =>  'spice_cookies_color_callback',
            'section'           =>  'spice_cookies_clr_section',
            'setting'           =>  'spice_cookies_btn_colorr'
        )
    ));

}
add_action( 'customize_register', 'spice_cookies_customizer_controls' );